import './App.css'
import HomePage from './Pages/HomePage'
import EffectPage from './Pages/EffectPage'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navbar from './Components/Navbar'
import GuitarsPage from './Pages/GuitarPage'
import CounterPage from './Pages/CounterPage'
import JokePage from './Pages/JokePage'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
      {/* put navbar here */}
      <Navbar />
      <Routes>
        {/* start with home page */}
        <Route path='/' element={<HomePage />} />
        {/* then other specific paths */}
        <Route path='/effects' element={<EffectPage />} />
        <Route path='/guitars' element={<GuitarsPage />} />
        <Route path='/counter' element={<CounterPage />} />
        <Route path='/jokes' element={<JokePage />} />
        {/* lastly, a 404 page */}
        <Route path='*' element={<h1>Sorry, this page doesn't exist.</h1>} />
      </Routes>

      </div>
    </BrowserRouter>
  )
}

export default App
