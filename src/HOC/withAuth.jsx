import { Navigate } from "react-router-dom"
import { UserContext } from "../Context/UserContext"
import { useContext } from "react"

const withAuth = (Component) => (props) => {
    const [currentUser, ] = useContext(UserContext)

    // login authentication logic (if)
    const isLoggedIn = currentUser != null

    if (isLoggedIn) {
        // user is logged in
        return <Component {...props} />
    } else {
        // user is NOT logged in
        // redirect them (home page)
        return <Navigate to="/" />
    }
    
}

export default withAuth