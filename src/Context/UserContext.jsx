import { createContext, useState } from "react"

export const UserContext = createContext(null)

const UserProvider = (props) => {

    const [currentUser, setCurrentUser] = useState(null)

    return (
        <UserContext.Provider value={ [currentUser, setCurrentUser] }>
            { props.children }
        </UserContext.Provider>
    )
}

export default UserProvider