import { createSlice } from "@reduxjs/toolkit"


export const counterSlice = createSlice({
    name: "counter",
    initialState: {
        value: 0,
        doubledValue: 0,
        tripledValue: 0,
    },
    reducers: {
        // actions
        increment: state => {
            state.value += 1
        },
        decrement: state => {
            state.value -= 1
        },
        incrementByValue: (state, action) => {
            state.value += action.payload.firstNum
            console.log(action.payload);
        },
    }
})

export const { increment, decrement, incrementByValue } = counterSlice.actions
export default counterSlice.reducer