import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { JOKE_API } from "../utils"

// thunk
export const getJokeAsync = createAsyncThunk(
    "joke/getJokeAsync",
    async () => {
        // console.log("fetching data from async thunk");
        const response = await fetch(JOKE_API)
        if (response.ok) {
            // console.log("fetched a response: OK");
            const json = await response.json()
            return json
        } else {
            console.log("There was an error.")
        }
    }
)

// slice
export const jokeSlice = createSlice({
    name: "joke",
    initialState: {
        setup: "Part A",
        delivery: "Part B"
    },
    reducers: {
        // empty
    },
    extraReducers: {
        [getJokeAsync.fulfilled]: (state, action) => {
            // console.log(action.payload);
            state.setup = action.payload.setup
            state.delivery = action.payload.delivery
        }
    }
})

export default jokeSlice.reducer