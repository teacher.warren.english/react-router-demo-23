import { configureStore } from "@reduxjs/toolkit"
import countReducer from "./counterSlice.js"
import jokeReducer from './jokeSlice.js'

export default configureStore({
    reducer: {
        // todo: reference reducer(s) from slice
        countReducer,
        jokeReducer,
    }
})