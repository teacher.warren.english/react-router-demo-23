export const API_URL = "https://randomuser.me/api/"
export const GUITAR_API_URL = "https://seasoned-pushy-kingfisher.glitch.me/guitars"

export const JOKE_API = "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart"