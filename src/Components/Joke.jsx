import { useDispatch, useSelector } from "react-redux"
import { getJokeAsync } from "../Redux-Parts/jokeSlice"
import { useEffect, useState } from "react"

const Joke = () => {

    // HOOKS
    // set up the useSelector when we're ready for that
    const joke = useSelector(state => state.jokeReducer)
    const dispatch = useDispatch()

    const [showPunchline, setShowPunchline] = useState(false)

    // HANDLER FUNCTIONS

    useEffect(() => {
        handleGetNewJokeAsync()
    }, []) // only run 1
    
    const handleGetNewJokeAsync = () => {
        // console.log("dispatch async thunk");
        setShowPunchline(false)
        dispatch(getJokeAsync()) // use the thunk, not a reducer action || NEEDS PARENTHESIS
    }

    const handleShowPunchline = () => setShowPunchline(true)

    return (
        <>
            <p onClick={ handleShowPunchline }>{ joke.setup }</p>
            { showPunchline && <p>{ joke.delivery }</p>}
            <button onClick={ handleGetNewJokeAsync }>Get a new joke</button>
        </>
    )
}

export default Joke