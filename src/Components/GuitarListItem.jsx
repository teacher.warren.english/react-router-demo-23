import './GuitarListItem.css'

// {currentGuitar} is destructuring 'props', so that in our component we don't use 'props.currentGuitar...' everywhere
const GuitarListItem = ({ currentGuitar }) => {
    // local variables
    let currentGuitarMaterials = []

    const renderGuitarMaterials = () => {
        // populate the array of currentGuitarMaterials using a for...in loop
        for (const key in currentGuitar.materials)
            currentGuitarMaterials.push(`${key}: ${currentGuitar.materials[key]}`)

        return currentGuitarMaterials.map((item, index) => <li key={index}>{item}</li>)
    }

    return (
        <div className='guitar-card'>
            <h2>{currentGuitar.manufacturer} {currentGuitar.model}</h2>
            <img src={currentGuitar.image} alt={currentGuitar.model} className='guitar-img' />
            <p>{currentGuitar.model}</p>
            <p>Materials:</p>
            <ul>{renderGuitarMaterials()}</ul>
        </div>
    )
}
export default GuitarListItem