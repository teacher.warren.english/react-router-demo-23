import { NavLink } from "react-router-dom"
import "./Navbar.css"

const Navbar = () => {

    return (
        <nav>
            <ul>
                <li><NavLink to="/">Home</NavLink></li>
                <li><NavLink to="/effects">Effects</NavLink></li>
                <li><NavLink to="/guitars">Guitars</NavLink></li>
                <li><NavLink to="/counter">Counter</NavLink></li>
                <li><NavLink to="/jokes">Jokes</NavLink></li>
            </ul>
        </nav>
    )
}
export default Navbar