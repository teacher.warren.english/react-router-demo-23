import { useContext, useEffect, useState } from "react"
import { UserContext } from "../Context/UserContext"

const Effect = () => {

    const [count, setCount] = useState(0)
    const [displayMessage, setDisplayMessage] = useState(false)
    const [currentUser,] = useContext(UserContext)

    useEffect(() => {
        console.log("useEffect() triggered!")

        if (count != 0)
            setDisplayMessage(true)

        setTimeout(() => {
            setDisplayMessage(false)
        }, 500)

    }, [count])

    const handleIncreaseCount = () => setCount(count + 1)

    return (
        <>
            { currentUser ? <p>{`${currentUser.name.title} ${currentUser.name.first} ${currentUser.name.last}`}</p> : <p>User is not logged in...</p>}
            <h1>Counter: {count}</h1>
            <button onClick={handleIncreaseCount}>Click me!</button><br />
            {displayMessage && <small>You changed the counter! 🚨</small>}
        </>
    )
}
export default Effect