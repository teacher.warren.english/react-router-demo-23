import { useDispatch, useSelector } from "react-redux"
import { decrement, increment, incrementByValue } from "../Redux-Parts/counterSlice"

function Counter() {

    // HOOKS
    const count = useSelector(state => state.countReducer.value)
    const dispatch = useDispatch()

    // Handlers
    const handleDecrease = () => {
        dispatch(decrement())
    }

    const handleIncrease = () => {
        dispatch(increment())
    }

    const handleIncreaseByValue = () => {
        dispatch(incrementByValue({ firstNum: 100, secondNum: 300 }))
    }

    return (
        <>
            <h2>Your count is: { count }</h2>
            <button onClick={ handleDecrease }>Decrease</button>
            <button onClick={ handleIncrease }>Increase</button>
            <button onClick={ handleIncreaseByValue }>Increase by 100</button>
        </>
    )
}

export default Counter