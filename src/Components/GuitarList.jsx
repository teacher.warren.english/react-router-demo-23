import './GuitarList.css'
import { useContext, useEffect, useState } from "react"
import { GUITAR_API_URL } from "../utils"
import GuitarListItem from "./GuitarListItem"
import { UserContext } from '../Context/UserContext'

const GuitarList = () => {
    // useState hook to store list of guitars locally
    const [guitarList, setGuitarList] = useState([])
    const [currentUser, ] = useContext(UserContext)

    // useEffect hook to trigger fetching the guitar data
    useEffect(() => {
        // cannot use fetch() in the top level of useEffect hook; nest it in a function
        const fetchGuitars = () => {
            fetch(GUITAR_API_URL)
                .then(response => response.json())
                .then(json => setGuitarList(json))
                .catch(error => console.log(error.message))
        }

        // invoke the fetchGuitars function
        fetchGuitars()
    }, []) // empty list of dependencies, only runs once

    // function to create a list of individual <GuitarListItem /> components
    const createGuitarList = () => {
        return guitarList.map((guitar) => <GuitarListItem currentGuitar={ guitar } key={ guitar.id } />)
    }

    return (
        <div className='guitar-list-container'>
            <p>{`${currentUser.name.title} ${currentUser.name.first} ${currentUser.name.last}`}</p>
            { createGuitarList() }
        </div>
    )
}
export default GuitarList