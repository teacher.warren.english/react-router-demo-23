import './Profile.css'
import { useContext } from "react"
import { API_URL } from "../utils.js"
import { UserContext } from '../Context/UserContext.jsx'

const Profile = () => {
    // HOOKS
    // const [currentUser, setCurrentUser] = useState(null)
    const [currentUser, setCurrentUser] = useContext(UserContext)

    // useEffect - triggers a side effect to happen
    // useEffect(() => {
    //     handleFetch()
    // }, []) // only run once (component loads)
    
    const handleFetch = () => {
        // get data
        fetch(API_URL)
        .then(response => response.json())
        .then(json => {
            console.log(json)
            // process user data
            setCurrentUser(json.results[0])
            // uncomment the line of code below to test the UI when no user data is retrieved
            // setCurrentUser(undefined)
            })
            .catch(error => console.log(error.message))
    }

    const handleLogout = () => {
        setCurrentUser(null)
    }

    return (
        <>
        { currentUser ? <button onClick={handleLogout}>Logout</button> : <button onClick={handleFetch}>Login</button> }
        {currentUser ? <div>
            <h3>{ currentUser.name.first } { currentUser.name.last }</h3>
            <img src={ currentUser.picture.large } alt={ currentUser.login.uuid } className="profile-img" />
            <p>{ currentUser.email }</p>
        </div> : 
        <div>
            <h3>John Doe</h3>
            <img src="placeholder.jpeg" alt="placeholder profile image" className="profile-img" />
            <p>example@email.com</p>
        </div> }
        </>
    )
}

export default Profile