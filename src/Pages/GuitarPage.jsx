import GuitarList from "../Components/GuitarList"
import withAuth from "../HOC/withAuth"

const GuitarsPage = () => {
    return (
        <GuitarList />
    )
}
export default withAuth(GuitarsPage)